package com.example.thanos.loginandregister;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.os.Bundle;
import android.widget.EditText;

/**
 * Created by thanos on 30/11/15.
 */
public class Register extends AppCompatActivity {
    EditText ET_FIRST_AND_LAST_NAME, ET_NAME, ET_PASS;
    String first_and_last_name, login_name, login_pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.register);

        ET_FIRST_AND_LAST_NAME = (EditText) findViewById(R.id.first_and_last_name);
        ET_NAME = (EditText) findViewById(R.id.user_name);
        ET_PASS = (EditText) findViewById(R.id.user_pass);
    }

    public void userRegistration(View view) {

        first_and_last_name = ET_FIRST_AND_LAST_NAME.getText().toString();
        login_name = ET_NAME.getText().toString();
        login_pass = ET_PASS.getText().toString();
        String method = "login";
        BackGroundTask backgroundTask = new BackGroundTask(this);
        backgroundTask.execute(method, login_name, login_pass);
    }
}
